# Human vs. Zombies

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://se-java-hvz-backend.herokuapp.com/swagger-ui/index.html)
[![container](https://img.shields.io/static/v1?logo=docker&message=Registry&label=Docker&color=2496ED)](https://gitlab.com/shuhia/se-java-hvz-1/container_registry)

Case application for Humans vs. Zombies

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

Humans vs.  Zombies (HvZ) is a game of tag played at schools, camps, neighborhoods,libraries, and conventions around the world. The game simulates the exponential spread of a fictional zombie infection through a population.
The game is played as follows:

- The  game  begins  with  one  or  more  “Original  Zombies”  (OZ)  or patient zero.The purpose of the OZ is to infect human players by tagging them. 
Once tagged, a human becomes a zombie for the remainder of the game.
- Human players are able to defend themselves against the zombie horde using Nerf weapons and clean, rolled-up socks which may be thrown to stun an unsuspecting zombie.
- Many  variants  of  the  game  rules  exist  which  introduce  additional  rules  and  activities  into  the  game; 
for  example, the  Rhodes  University  variant  introduces  a web-based game map where markers appear showing the location of missions and supplies.  The appearance of supplies forces humans to leave their safe zones and risk being turned.

While HvZ is a highly physical and active game, the administrators would have difficulty communicating with all the players and keeping track of the game and individual player states without help. 
For this case, we were tasked with designing and implementing a system that will enable players to manage their own state in accordance with the rules of the game and leave the administrators free to focus on improving the game itself.

### Built With

 - Java/ Spring Boot
 - Spring Security
 - Hibernate
 - Keycloak
 - Postgres SQL
 - Docker

## Getting Started

### Prerequisites

[Intellij IDE](https://code.visualstudio.com/) or any other editor.

### Installation

1. Clone the repository with the following command

```
    git clone https://gitlab.com/shuhia/se-java-hvz-1.git
```

2. Open up the project in your editor. When starting, Gradle will automatically initialize itself and download necessary dependencies the first time the wrapper is run. No explicit installation necessary.


3. The .env file at the root of the project, remove the .example extension from the file name as explained in [deploying postgres and keycloak](#deploying-postgres-and-keycloak) down below.


4. Run docker compose to build the container for your database.


5. Start your Spring Boot Application.

### Setting up Keycloak

When you have done all the instructions that is located in [installation](#installation) - 
You have to follow this steps to set up your authentication service that is going to generate JSON Web Token(JWT) to authenticate the user against the application:

1. Start your application and navigate your self to [keycloak admin console](http://localhost:8083/auth/) at this link. Hit the "Administration Console" to get started.
2. At initially, The keycloak has not a default admin user. Hence you have to create an admin user. Enter Username, Password, Confirm Password to create an admin user for keycloak.
3. Click Administration Console and log with above username and password.
4. When you have successfully logged into the admin console, look at the menu tab and navigate to "Clients". 
In here click on the create button located to the top right of the table of clients and create a new one called "client-id".

<img src="images/create-client.PNG" alt="drawing" width="700"/>
<img src="images/client-id.PNG" alt="drawing" width="640"/>

5. When created, first enable "Implicit flow", to be able to redirect without authentication for anonymous user. Then navigate to section called "Valid Direct URI" and put in your local instance of your application (localhost:8080/*) and hit the plus button, then save at the bottom of the form.
This will make sure that keycloak will redirect the user back to the application on successful login.
   After you've done that, navigate down to web-origin section and allow cors-origin for all requests by adding a "+" to the input. Don't forget to save to not lose the settings.

<img src="images/client-id-final.PNG" alt="drawing" width="500"/>

6. Head over to the "Mappers" tab and add a mapper to insert the roles in to the JWT, hit create and name it "client-roles".
The mapper type should be set to "User Client Role" and map it to "client-id" on the section below the name.
Then set the token claim name to "roles" and finally set the claim JSON type to string and hit save.

<img src="images/client-role-mapper.PNG" alt="drawing" width="500"/>

7. Navigate to the "Roles" tab on the client menu and add some roles that will be added to the JWT to authenticate the user for certain requests. For this application, you need to create "user" and "admin".

<img src="images/client-roles.PNG" alt="drawing" width="900"/>

8. Next is to add users to the system, this can be done by navigating to the "Users" tab on the menu. Then click "Add user" to create a user.

<img src="images/users.PNG" alt="drawing" width="900"/>

9. After you have created a user, navigate to "Role Mappings" on the menu. In here, you can set what role this user will have in the application.
At the bottom, click on the "Client Roles" list and select "client-id" that you have created. 
You should see the roles that you created, select which one you want to add for this user and press "Add Selected".

<img src="images/user-roles-added.PNG" alt="drawing" width="900"/>

10. Now everything should be setup for testing the application.

### Deploying Postgres and Keycloak

Postgres and Keycloak is deployed using Docker, using the Docker Compose configuration `docker-compose.yml`. This is setup to accept four environment variables:

- `POSTGRES_DB` &mdash; the name of the postgres database
- `POSTGRES_PASSWORD` &mdash; environment variable that is strictly required by the Postgres image.
- `KEYCLOAK_USER` &mdash; the name of the keycloak admin for the realm
- `KEYCLOAK_PASSWORD` &mdash; environment variable that is strictly required by the Keycloak image.

These are specified in a file that you must create called `.env` (without file extension). This is done by copying `.env.example` and modify values to your preference.

You can then start the Postgres server by running the following command:

```
    docker-compose up -d backend
```



## Usage

To test out the API for this application locally, head to the Swagger/OpenApi interface and interact with the API endpoints
- [Swagger UI interface (Localhost)](https://localhost:8080/swagger-ui/index.html)

When testing the endpoints, nearly all requests are restricted from access unless the user is authenticated.
To authorize the requests, hit the "Authorize" button and check the "openid" box and click on "Authorize".
This will make sure that you have logged in with the authenticator (Keycloak) and retrieved a JWT that is used for the request header.

There are a couple of instances where you will receive an error code:

- 401 - this means that you have not been authenticated, follow the instructions in the previous section.
- 403 - this means that the user you have logged in with, doesn't have the right authorities to access the request.
Check your keycloak settings for that particular user so it has the right roles.

For more information about this, look at the API-documentation.md file in this repository.


## Demo

To test out the deployed version of the API, head to the Swagger/OpenApi interface and interact with the API endpoints
- [Swagger UI interface (Deployed version)](https://se-java-hvz-backend.herokuapp.com/swagger-ui/index.html)



## Maintainers

- [Alex On (@shuhia)](https://gitlab.com/shuhia)
- [Christopher Vestman (@druwan)](https://gitlab.com/druwan)
- [Nils Jacobsen (@nils_jacobsen)](https://gitlab.com/nils_jacobsen)
- [Sebastian Börjesson (@sebastian.borjesson)](https://gitlab.com/sebastian.borjesson)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

