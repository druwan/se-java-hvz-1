package se.java.hvz.Controllers;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import se.java.hvz.Models.*;
import se.java.hvz.Models.dto.PlayerDto;
import se.java.hvz.Repositories.ChatRepository;
import se.java.hvz.Repositories.GameRepository;
import se.java.hvz.Repositories.KillRepository;
import se.java.hvz.Repositories.PlayerRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/game")
@SecurityRequirement(name = "keycloak_implicit")
public class PlayerController {

    @Autowired
    KillRepository killRepository;
    @Autowired
    GameRepository gameRepository;
    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    ChatRepository chatRepository;

    // GET /game/<game_id>/player
    // Get a list of players. Player details only show limited properties.
    // This to not show sensitive information for the user that is not an administrator
    @GetMapping("/{game_id}/player")
    @PreAuthorize("hasRole('user')")
    public ResponseEntity<CommonResponse<List<PlayerDto>>> getAllPlayersByGameId(@PathVariable Long game_id) {
        try {
            return ResponseEntity
                    .ok(new CommonResponse<>(playerRepository.findByGameGameId(game_id)));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // GET /game/<game_id>/player/admin
    // Get a list of players. Returns the entire player object as admin
    @GetMapping("/{game_id}/player/admin")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<CommonResponse<List<Player>>> getAllPlayersByGameIdAndIsAdmin(@PathVariable Long game_id) {
        try {
            return ResponseEntity
                    .ok(new CommonResponse<>(playerRepository.findAllByGameGameId(game_id)));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // GET /game/<game_id>/player/<player_id>
    // Returns a specific player object.
    @GetMapping("/{game_id}/player/{player_id}")
    @PreAuthorize("hasRole('user')")
    public ResponseEntity<CommonResponse<Player>> getPlayerByGameId(@PathVariable Long game_id, @PathVariable Long player_id) {
        try {
            Player player = playerRepository.findPlayerByPlayerId(player_id);
            if (player == null) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "No player found."));
            }

            return ResponseEntity
                    .ok(new CommonResponse<>(playerRepository.findPlayerByGameGameIdAndPlayerId(game_id, player_id)));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // GET /game/<game_id>/player/<player_name>
    // Returns a specific player object.
    @GetMapping("/{game_id}/player/name/{player_name}")
    @PreAuthorize("hasRole('user')")
    public ResponseEntity<CommonResponse<Player>> getPlayerByGameIdAndPlayerName(@PathVariable Long game_id, @PathVariable String player_name) {
        try {
            Player player = playerRepository.findPlayerByGameGameIdAndPlayerName(game_id, player_name);
            if (player == null) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "No player found."));
            }

            return ResponseEntity
                    .ok(new CommonResponse<>(playerRepository.findPlayerByGameGameIdAndPlayerName(game_id, player_name)));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // POST /game/<game_id>/player
    // Registers a user for a game.
    @PostMapping("/{game_id}/player")
    @PreAuthorize("hasRole('user')")
    public ResponseEntity<CommonResponse<Player>> registerUserToGame(@PathVariable Long game_id, @RequestBody Player player) {
        try {
            Game game = gameRepository.findById(game_id).get();
            if (game.getState().equals("Completed")) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The game is completed. No further actions can be taken"));
            }
            player.setGame(game);
            player.setHuman(true);
            player.setPatientZero(false);
            if (playerRepository.findPlayerByGameGameIdAndPlayerName(game_id, player.getPlayerName()) != null) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "Registration for this game is only allowed once."));
            }
            if (game.getState().equals("In Progress")) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "Game is in progress. No registration allowed."));
            }
            playerRepository.save(player);
            if (game.getPlayers().size() >= game.getPlayerLimit()) {
                game.setState("In Progress");
                gameRepository.save(game);
                List<Player> players = game.getPlayers();
                int random = (int) (Math.random() * players.size());
                Player playerToBePatientZero = players.get(random);
                playerToBePatientZero.setPatientZero(true);
                playerToBePatientZero.setHuman(false);
                playerRepository.save(playerToBePatientZero);
            }

            return ResponseEntity
                    .ok(new CommonResponse<>(player));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // PUT /game/<game_id>/player/<player_id>
    // Updates a player object. Admin only.
    @PutMapping("/{game_id}/player/{player_id}")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<CommonResponse<Player>> updatePlayerByGameId(@PathVariable Long game_id, @PathVariable Long player_id, @RequestBody Player playerUpdate) {
        try {
            Game game = gameRepository.findById(game_id).get();
            if (game.getState().equals("Completed")) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The game is completed. No further actions can be taken."));
            }
            Player player = playerRepository.findPlayerByGameGameIdAndPlayerId(game_id, player_id);
            if (player == null) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "No player found."));
            }
            player.setHuman(playerUpdate.getHuman());

            return ResponseEntity
                    .ok(new CommonResponse<>(playerRepository.save(player)));

        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }

    // DELETE /game/<game_id>/player/<player_id>
    // Deletes (cascading) a player. Admin only.
    @DeleteMapping("/{game_id}/player/{player_id}")
    @PreAuthorize("hasRole('user')")
    @Transactional
    public ResponseEntity<CommonResponse<Player>> removePlayerFromGameByPlayerId(@PathVariable Long game_id, @PathVariable Long player_id) {
        try {
            Game game = gameRepository.findById(game_id).get();
            if (game.getState().equals("Completed")) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "The game is completed. No further actions can be taken."));
            }
            Player deletedPlayer = playerRepository.findPlayerByGameGameIdAndPlayerId(game_id, player_id);
            if (deletedPlayer == null) {
                return ResponseEntity
                        .ok(new CommonResponse<>(400, "No player found."));
            }

            killRepository.deleteKillByVictimOrKiller(deletedPlayer, deletedPlayer);
            playerRepository.deletePlayerByGameGameIdAndPlayerId(game_id, player_id);

            return ResponseEntity
                    .ok(new CommonResponse<>(deletedPlayer));
        } catch (NoSuchElementException e) {
            return ResponseEntity
                    .ok(new CommonResponse<>(400, "No game found."));
        }
    }
}
