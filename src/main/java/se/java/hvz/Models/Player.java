package se.java.hvz.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Random;

@EntityListeners(PlayerEventListener.class)
@Entity
@Table(name = "tbl_players")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long playerId;

    private String playerName;

    @Column(name = "isPatientZero", columnDefinition = "boolean default false")
    private boolean isPatientZero = false;

    @Column(name = "isHuman", columnDefinition = "boolean default true")
    private boolean isHuman = true;

    private String biteCode;

    @ManyToOne
    @JoinColumn(name = "game_id")
    @JsonIgnore
    private Game game;

    public Player() {
    }

    public Player(Long playerId) {
        this.playerId = playerId;
    }

    public Player(Long playerId, String playerName, Boolean isPatientZero, Boolean isHuman, String biteCode, Game game) {
        this.playerId = playerId;
        this.playerName = playerName;
        this.isPatientZero = isPatientZero;
        this.isHuman = isHuman;
        this.biteCode = biteCode;
        this.game = game;
    }


    // Randomizer for creating bitecodes for every user
    // that register for a game
    @PostPersist
    protected void onCreate() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 6;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString().toUpperCase();
        setBiteCode(generatedString);

    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Boolean getPatientZero() {
        return isPatientZero;
    }

    public void setPatientZero(Boolean patientZero) {
        isPatientZero = patientZero;
    }

    public Boolean getHuman() {
        return isHuman;
    }

    public void setHuman(Boolean human) {
        isHuman = human;
    }

    public String getBiteCode() {
        return biteCode;
    }

    public void setBiteCode(String biteCode) {
        this.biteCode = biteCode;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
}
