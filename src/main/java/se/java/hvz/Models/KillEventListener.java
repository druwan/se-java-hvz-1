package se.java.hvz.Models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

@Component
public class KillEventListener {

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @PostPersist
    @PostUpdate
    private void afterAnyUpdate(Kill kill) {
        String game_id = String.valueOf(kill.getGame().getGameId());
        this.simpMessagingTemplate.convertAndSend("/topic/game/"+game_id+"/kill", kill);
    }
}
