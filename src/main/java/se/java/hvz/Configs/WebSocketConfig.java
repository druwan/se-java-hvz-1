package se.java.hvz.Configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        // Enable broker for topic. A topic will be broadcasted to all subscribers in the topic.
        config.enableSimpleBroker("/topic");
        // Sets prefix for socket.
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        // Endpoint for the client to connect to in order to aquire a websocket connection
        registry.addEndpoint("/ws").setAllowedOrigins("http://localhost:4200","https://se-java-hvz-frontend.herokuapp.com/","https://se-java-hvz-frontend.netlify.app/").withSockJS();
    }

}